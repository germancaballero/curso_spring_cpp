
#include "polimorfismo.h"

using namespace curso;

void casting() {
    // Polimorfismo b�sico: casting
    Coche *miCoche = new Coche(4);
    miCoche->Arrancar();
    miCoche->Mostrar(); // Aqu� llama al propio de coche
    miCoche->SetPuertas(2);
    miCoche->AbrirPuertas();

    Vehiculo *miVehiculo;
    miVehiculo = miCoche;   // Casting impl�cito
            // Podemos convertir un coche en veh�culo
            // nuestro coche tiene la forma de veh�culo
            // Simplemente apunta a la misma direcci�n de memoria
            // podemos acceder a las propiedadees de Vehiculo
    miVehiculo->Apagar();
    miVehiculo->IndicarLuces();
    miVehiculo->Mostrar();// Aqu� llama al propio de Vehiculo
    // Pero NO a las de Coche, mientras tenga esta forma
    // miVehiculo->SetPuertas(4);   //ERROR

    // Adem�s podemos recuperar su forma original:
    Coche *elMismoCoche;
    // Pero directamente no podemos convertir un vehiculo coche
    // elMismoCoche = miVehiculo;  // ERROR
    // PERO SI EL MISMO TIPO, s� podemos hacer la conversi�n expl�cita
    elMismoCoche = (Coche*) miVehiculo;
    // Una vez retornada la forma, SIGUE siendo un coche OK
    cout << "Despues de haberse reconvertido:\n";
    elMismoCoche->AbrirPuertas();
    delete miCoche;
}

void EjercicioMoto() {
    // Crear una moto, y darle forma de vehiculo,
    Vehiculo *vehMoto = new Moto();

    // arrancar y acelerar,
    vehMoto->Arrancar();
    vehMoto->Acelerar(5);
    vehMoto->Mostrar();

    // volver a darle forma de moto,
    Moto *vecMoto = (Moto *) vehMoto;
    // apagar, y mostrar
    vecMoto->Apagar();
    vecMoto->Mostrar();
    delete vehMoto;
}

void sobreescrituraMetodosVirtuales()
 {
    Coche *limusina = new Coche(8);
    limusina->SetLuces(true);
    limusina->IndicarLuces();

    Vehiculo *vehLimus= limusina;
    vehLimus->IndicarLuces();
    delete limusina;
}
// Ejercicio: COnvertir el m�todo Arrancar en virtual e implementarlo s�lo en la moto: Adem�s de cambiar el booleano tiene que mostrar
// "Brrrruuumm brum!!"
void EjercicioMotoVirutalMet() {
    Moto *kawasaki = new Moto();
    kawasaki->Arrancar();
    kawasaki->Mostrar();
    Vehiculo *kawaVeh = kawasaki;
    kawaVeh->Arrancar();
    kawaVeh->Mostrar();

    delete kawasaki;
    // Si quiseramos, podr�amos invocar al m�todo del padre
}
void PruebaITV(Vehiculo* vehiculo);

void pruebasITVs()
{
    Moto *cbr = new Moto();
    Coche *kia = new Coche(4);
    // No es posible, porque Vehiculo es una clase abstracta
    // Vehiculo *vehiculoAleat = new Vehiculo(4);
    PruebaITV(cbr);
    PruebaITV(kia);
    delete cbr;
    delete kia;
}
void arraysVehiculos()
{
    Vehiculo* miGaraje[4];
    miGaraje[0] = new Coche(4);
    miGaraje[1] = new Coche(8);
    miGaraje[2] = new Coche(4);
    miGaraje[3] = new Moto();

    for (int i = 0; i < 4; i++) {
        PruebaITV(miGaraje[i]);
        miGaraje[i]->Mostrar();
        delete miGaraje[i];
    }
}

void PruebaITV(Vehiculo* vehiculo)
{
    cout << "ITV: Espere 30 min" << endl;

    // Primero comprobamos si es de tipo coche / moto
    // No funciona correctamente: Investigar
    /*if (typeid(Coche).name() == typeid(vehiculo).name())
    {
        cout << "Tu vehiculo es un coche" << endl;
    }*/
    if (vehiculo->GetTipo() == TCoche){
        cout << "Tu vehiculo es un coche" << endl;
    } else if (vehiculo->GetTipo() == TMoto){
        cout << "Tu vehiculo es una moto" << endl;
    }

    vehiculo->Arrancar();
    vehiculo->Acelerar(10);
    vehiculo->Mostrar();
    if (vehiculo->GetLuces()) {
            cout << "Luces OK\n";
    }
    vehiculo->IndicarLuces();
    vehiculo->Apagar();
    vehiculo->Mostrar();
    cout << "Fin ITV" <<endl;
}
void pruebaCocheElectrico() {
    CocheElectrico *tesla = new CocheElectrico(20);


    cout << "Nivel de bateria: "
         << tesla->GetNivelBateria()
         << endl;

    tesla->CargarBateria(100);
    cout << "Nivel de bateria: "
         << tesla->GetNivelBateria()
         << endl;
    tesla->Arrancar();
    tesla->Acelerar(30);
    tesla->Mostrar();

    delete tesla;
}
