#include <iostream>
#include <vector>
#include "Coche.h"
#include "CocheElectrico.h"
#include "Moto.h"
#include "polimorfismo.h"

using namespace std;
using namespace curso;

void creacionCoches(void);
void creacionCochesConPtros(void);
void motosYCoches(void);

void funcionCallback(int);
void haceMovidasUIoBD_Etc(char *texto, void (*ptr_functCB)(int));

int main()
{
   std::cout << "Hola mundo!" << std::endl;
   // System.out.println ("Hola mundo!" + "\n");

   //creacionCoches();
    // cout << endl << "Ahora con punteros: " << endl;
   //creacionCochesConPtros();

   //motosYCoches();
   //casting();
   //EjercicioMoto();
   //sobreescrituraMetodosVirtuales();
   //EjercicioMotoVirutalMet();
   //pruebasITVs();
   //arraysVehiculos();
   pruebaCocheElectrico();

   // Punteros a funcioones
   char *miTexto = "MI TEXTO DE MI PROGRAMA";
    // Vamos a usar la librer�a externa
    void (*ptr_funcion) (int) = funcionCallback;
    haceMovidasUIoBD_Etc(miTexto, ptr_funcion);

   getchar();
   return 0;
}

// Ejercicio: A�adir campo velocidad (float) y encendido (bool), constructores, getters & setter...
// M�todo Acelerar: Que reciba param fuerza, y si est� encendido el coche, sume a la vel la 10 % de la fuerza ( fuerza / 10)
// Usarlos con coches normales y con punteros

// Ejercicio:
// 0- Nueva funcion en polimorfismo (pruebaCocheElectrico)
// 1 - Nueva clase, coche el�ctrico,
// 2 - Nueva propiedad: bater�a (nivel de bater�a) privada
// 3 - con un �nico constructor que reciba el nivel de bater�a (Amperios), siempre de 4 ruedas.
// 4 - En el destructor, que ponga "reciclando bater�a"
// 5 - Nuevos m�todos CargarBateria() y GetNivelBateria()
// 6 - Sobreescribir como m�todo virtual Acelerar, que vaya
//     la velocidad a la mitad de la fuerza (en vez de 10�)
// 7 - Sobreescribir el m�todo Mostrar() para indicar que es coche el�ctrico, el nivel de bater�a y los mismos datos que Coche (es decir, invocar al m�todo del padre)

void creacionCoches(void)
{
   // Declaraci�n de variable tipo clase,
   // gestionada por el compilador,
   // el compilador crea el c�digo para
   // instanciar el objeto (reservar la memoria) y para liberar liberar
   // Como si fuera una estructura
   curso::Coche miCoche;
   miCoche.Mostrar();

   curso::Coche mustang(4);

   // En C (o C++) cuando declaramos un objeto normal, como estos, cuando se copia no se copia una referencia como en Java, si no que se CLONA
   // coche y mustang son diferentes diferentes espacios en memoria
   mustang = miCoche;

   miCoche.SetLuces(true);

   mustang.IndicarLuces();
}

void creacionCochesConPtros(void)
{
   // Declaraci�n de variable tipo puntero
   Coche *miCoche;
   miCoche = new Coche();  // new devuelve un puntero
   miCoche->Mostrar();

   Coche *mustang;

   // Ahora s� que referenciamos al mismo objeto (esp. mem.)
   // mismo coche
   mustang = miCoche;

   miCoche->SetLuces(true);

   miCoche->Mostrar();
   mustang->IndicarLuces();

   // Que no se nos olvide liberar la memoria
   miCoche->Acelerar(100);
   miCoche->Mostrar();

   miCoche->Arrancar();
   miCoche->Acelerar(100);
   miCoche->Mostrar();
   delete miCoche;
   // Despues de un delete, no nos avisa de que
   // este puntero est� mal, apunta a un espacio
   // aleatorio con datos aleatorios. Puede cascar
   // miCoche->Acelerar(200);
   miCoche->Mostrar();
   cout << endl << "Fin demo coches ";
}
void motosYCoches() {
   // Polimorfismo: Un objeto puede tener su forma o la de sus clases padre
   // pero nunca la de una clase hija
   // Moto puede tener la forma de Vehiculo, pero un veh�culo no puede la forma de una moto o un coche
   Moto *motillo = new Moto();

   motillo->IndicarLuces();
   motillo->Arrancar();
   motillo->Acelerar(150);
   motillo->Mostrar();
   motillo->IndicarLuces();

   motillo->Estrellarse();

   Vehiculo *motoFormaVeh = motillo;
   // Ahora s�lo podemos usar los m�todos del padre
   motoFormaVeh->Mostrar();
   // motoFormaVeh->Estrellarse();

   delete motillo;
}

void ejemploLista() {
    vector<int> *lista = new vector<int>();
    lista->push_back(1);
    lista->push_back(2);
    vector<Coche*> *lista2 = new vector<Coche*>();
    lista2->push_back(new Coche(6));
}

void funcionCallback(int valor) {
    cout << "YO SOY la cllbk: " << valor << endl;
}
// OTRA FUNCION DE UN SISTEMA EXTERNO:
// Progamado en un sistema de bbdd, llamada a UI, etc,
// de acceso a servidores: Otro programador
void haceMovidasUIoBD_Etc(char *texto, void (*ptr_functCB)(int)) {
    cout << "Hago Movidas UI o BD" << endl;
    cout << "Muestro: " << texto << endl;
    ptr_functCB(100);   // Porque haya leido 100 registros
                        // Porque haya pulsado en la posici�n 100
}

