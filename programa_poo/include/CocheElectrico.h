#ifndef COCHEELECTRICO_H
#define COCHEELECTRICO_H

#include "Coche.h"

namespace curso {
class CocheElectrico : public Coche
{
    public:
        CocheElectrico(float);
        virtual ~CocheElectrico();

        void Acelerar(float fuerza) {
           if (this->encendido) {
                this->velocidad += fuerza / 2;
           }
        }
        void CargarBateria(float b) {
            this->bateria = b;
        }
        float GetNivelBateria() {
            return this->bateria;
        }
        void Mostrar();

    protected:

    private:
        float bateria;
};

}

#endif // COCHEELECTRICO_H
