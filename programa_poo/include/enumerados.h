#ifndef ENUMERADOS_H_INCLUDED
#define ENUMERADOS_H_INCLUDED

enum TipoVehiculo
{
    TCoche, // 0
    TMoto,   // 1
    TBicicleta, // 2
    // Podr�amos un valor
    TDesconocido = 999,
};

#endif // ENUMERADOS_H_INCLUDED
