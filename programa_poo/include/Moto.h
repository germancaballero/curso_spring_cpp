#ifndef MOTO_H
#define MOTO_H

#include <iostream>
#include "Vehiculo.h"

using namespace std;

namespace curso
{

// Como un coche, pero siempre con 2 ruedas y
// las encendidas SIEMPRE que la moto est� arrancada
class Moto : public Vehiculo
{
   public:
      Moto();
      virtual ~Moto();

      bool GetLuces() { return this->encendido; }

      void Estrellarse(){
         cout << " Estrellao" << endl;
      }
      virtual void Arrancar() {
         cout << "Brrrruuumm brum!!" << endl;
         // this->encendido = true;
         Vehiculo::Arrancar();
      }
   protected:

   private:
};
}
#endif // MOTO_H
