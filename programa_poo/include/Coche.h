#ifndef COCHE_H
#define COCHE_H

#include <iostream>
#include "Vehiculo.h"

using namespace std;

namespace curso
{

// Definimos la estructura y declaraci�n de la clase
// Incluyendo el nivel de acceso de variables miembro (propiedades) y m�todos (public, private...), el tipo de dato de las variables miembro, y la firma de los m�todos
class Coche : public Vehiculo // La herencia se hace con dos puntos
{
public:
   // Lo justo y necesario (imprescindible) para usar la clase
   Coche();
   // Los constructores SON los m�todos especiales que se
   // llaman cuando se crea un nuevo objeto
   Coche(int numRuedas);
   // Declaramos el destructor, que ES el m�todo especial que
   // se llama cuando se libera la memoria de un objeto
   ~Coche();

   // Estos m�todos implementados aqu�, son de tipo inline, es decir,
   // No se compilan y se usan como los m�todos el los ficheros CPP, si no que se sustituyen all� donde se usan, incrustando directamente el c�digo binario
   // En resumen: Ocupan m�s memoria, pero son m�s r�pidos

    bool GetLuces() { return this->luces; }
    bool SetLuces(bool luces) { this->luces = luces;  }
    int GetPuertas() { return this->puertas; }
    void SetPuertas(int p) {this->puertas = p; }
    void AbrirPuertas();

    // Sobreescritura: intentamos "machacar" el m�todo del padre con uno propio. Para que se produzca, el m�todo del padre tiene que tener el mismo nombre Y PAR�METROS

    // Sobreescritura standard, normal
    void Mostrar();

    // Sobreescritura virtual
    void IndicarLuces();

protected:
   // Si hubiera herencia, protected
private:
   // Por norma general, todo lo haremos privado, sobre todo
   // las variables miembro
   bool luces;
   int puertas;
};

}
#endif // COCHE_H
