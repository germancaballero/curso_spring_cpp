#ifndef VEHICULO_H
#define VEHICULO_H

#include "enumerados.h"
#include <iostream>

using namespace std;

namespace curso
{
    // Esta clase es abstracta de facto: Porque tiene un m�todo abstracto.
    // Estas clases no pueden instanciarse directamente, s�lo se pueden para
    // para que una clase derivada (hija) tenga su forma.
class Vehiculo
{
public:
   Vehiculo(int numRuedas);
   virtual ~Vehiculo();

   void Mostrar();
   // virutal normal, implementada
   virtual void IndicarLuces();
   // m�todo virtual puro: igualamos a CERO
   // Lo mismo que una funci�n abstract de Java
   virtual bool GetLuces() = 0;
   // Un m�todo virtual puro, es un m�todo abstracto,
   // es decir, est� declaro pero NO implementado

   virtual void Acelerar(float );// Aqu� lo declaramos
   // Sobrecarga de m�todos (o funciones) es cuando al mismo nombre le aplicamos diferentes par�metro
   void Acelerar() { this->Acelerar(10); }
   // Implementada en CPP como ejemplo
   // No hace falta ponerle nombres
   //  a los par�metros

   float GetVelocidad() { return this->velocidad; }
   virtual void Arrancar()      {
        this->encendido = true;
        cout << "Vehiculo arrancado\n";
    }
   void Apagar()        { this->encendido = false; }

   int GetRuedas() { return this->ruedas; }

   enum TipoVehiculo GetTipo() {
       return this->tipo;
   };

protected:
   // Miembros privados pero accesibles desde las clases hijas
   float  velocidad;
   bool encendido;
   // En el fondo es un int
   enum TipoVehiculo tipo;

private: // Ni siquiera las clases hijas podr�n acceder a estos miembros
   int8_t ruedas;
};
}
#endif // VEHICULO_H
