#ifndef POLIMORFISMO_H_INCLUDED
#define POLIMORFISMO_H_INCLUDED

#include <typeinfo>
#include "Coche.h"
#include "Moto.h"
#include "CocheElectrico.h"

void casting();
void EjercicioMoto();
void sobreescrituraMetodosVirtuales();
void EjercicioMotoVirutalMet();
void pruebasITVs();
void arraysVehiculos();
void pruebaCocheElectrico();

#endif // POLIMORFISMO_H_INCLUDED
