#include "Coche.h"

using namespace curso;
// Implementamos los constructores y los métodos
// El formato de los métodos/constructores es
//  NombreClase::NombreMetodo( parámetros) {
//       Implementación
// }
curso::Coche::Coche() : curso::Vehiculo(4)
{                     //  En este ctor usamos el del padre
    //ctor
    this->luces = false;
    this->tipo = TCoche;
}

Coche::Coche(int numRuedas): curso::Vehiculo(numRuedas)
{
    this->luces = false;
    this->tipo = TCoche;
}

void Coche::AbrirPuertas() {
    cout << "Abriendo " << this->puertas << " puertas.\n";
}

void Coche::IndicarLuces() {
    cout << "Coche con luces "
         << ((this->GetLuces())
             ? "prendidas"
             : "apagadas") << endl;
}
void Coche::Mostrar() {
    cout << "Coche con "
         << (int) this->GetRuedas()
         << " ruedas\n";
}

Coche::~Coche() {
   cout << " Aqui habria que liberar memoria  de otros objetos, arrays, cerrar conexiones..." << endl;
}


