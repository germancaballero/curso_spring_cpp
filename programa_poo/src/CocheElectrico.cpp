#include "CocheElectrico.h"

using namespace curso;

CocheElectrico::CocheElectrico(float bat)
    : Coche(4)
{
    this->bateria = bat;

}
CocheElectrico::~CocheElectrico()
{
    cout << "Reciclando bateria" << endl;
}
void CocheElectrico::Mostrar() {
    cout << "Coche electrico"
         << ", bater�a: " << bateria << endl;
    Coche::Mostrar();
    Vehiculo::Mostrar();
}
