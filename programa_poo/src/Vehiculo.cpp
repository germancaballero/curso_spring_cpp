#include "Vehiculo.h"

curso::Vehiculo::Vehiculo(int numRuedas)
{
   //ctor
    // Mientras que en Java, this es una referencia al objeto,
    // aqu� es un puntero
    this->ruedas = numRuedas;
    velocidad = 0;   // this no es obligatorio
    encendido = false;
    this->tipo = TDesconocido;
}
// Operador ternario: recibde 3 operandos:
//    condicion_bool ? resultado_true : resultado_false
void curso::Vehiculo::Mostrar() {
    cout << "Vehiculo con " << (int) this->ruedas
         << " ruedas "
         << ( this->encendido ? " arrancado " : " apagado " )
         << ( this->GetLuces() ? " con luces " : " sin luces " )
         << " a " << this->velocidad << " km/h."
         << endl;
}
void curso::Vehiculo::IndicarLuces() {
    if (this->GetLuces()) {
        cout << "Vehiculo con luces prendidas" << endl;
    } else {
        cout << "Vehiculo con luces apagadas" << endl;
    }
}
void curso::Vehiculo::Acelerar(float fuerza) {
   if (this->encendido) {
      this->velocidad += fuerza / 10;
   }
}
curso::Vehiculo::~Vehiculo()
{
   //dtor
   cout << " * Destruir vehiculo. " << endl;
}
