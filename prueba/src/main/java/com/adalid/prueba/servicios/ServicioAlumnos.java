package com.adalid.prueba.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adalid.prueba.modelo.daos.AlumnosDAO;
import com.adalid.prueba.modelo.entidades.Alumno;

// Se usas para implementar cualquier tipo de lógica de negocio, validaciones especiales, cruce datos,
// Llamar a otras APIs...
@Service
public class ServicioAlumnos 
{
	@Autowired 
	private AlumnosDAO alumnosDAO;
	
	public Alumno save(Alumno alumno) {
		if (alumno != null) {
			if (alumno.getEdad() >= 18) {
				return alumnosDAO.saveAndFlush(alumno);
			}
		}
		System.out.println("El alumno es menor de edad");
		return null;
	}
	public AlumnosDAO getDAO() {
		return alumnosDAO;
	}
}
