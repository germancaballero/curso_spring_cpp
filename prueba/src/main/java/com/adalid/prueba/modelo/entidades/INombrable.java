package com.adalid.prueba.modelo.entidades;

public interface INombrable {

	String getNombre();
	void setNombre(String nombre);
}
