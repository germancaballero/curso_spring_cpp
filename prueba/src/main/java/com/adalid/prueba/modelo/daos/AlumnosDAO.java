package com.adalid.prueba.modelo.daos;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adalid.prueba.modelo.entidades.Alumno;


/* Gestión de operaciones CRUD:
 * Create		Alta / Creación
 * Read			Consulta:		Puede haber muchas formas, p.ej. filtros
 * Update		Modificación
 * Delete		Baja / Borrado
 */
public interface AlumnosDAO
extends JpaRepository<Alumno, Integer>
{
	/*private ArrayList<Alumno> alumnos;
	
	public AlumnosDAO() {
		this.alumnos = new ArrayList<Alumno>();
	}
	public void crear(String nombre, int edad) {
		Alumno nuevoAlumno = new Alumno(nombre, edad);
		alumnos.add(nuevoAlumno);
	}
	public void crear(Alumno nuevoAlumno) {
		alumnos.add(nuevoAlumno);
	}
	public Alumno getAlumno(int indice) {
		Alumno alumnoBuscado = alumnos.get(indice);
		return alumnoBuscado;
	}
	public int size() {
		return this.alumnos.size();
	} */
}
