package com.adalid.prueba.modelo.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.lang.NonNull;


// POJO: Plain Old Java Object
// BEAN: POJO con un constructor por defecto, y serializable (Convertible a un formato
//  transmitible, XML, JSON, BB.DD....

@Entity
public class Alumno implements INombrable, Serializable  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull
	@Size(min = 1, max = 100)
	// @NonNull
	private String nombre;

	@NotNull
	@Size(min = 3, max = 255)
	@Column(unique = true)
	@Pattern(regexp = "^[^@]+@[^@]+\\.[a-zA-Z]{2,}$", message = "El email no está bien formado, ¡qué te pasa!")
	// @Email
	
	private String email;
	
	@Min(value = 1)
	@Max(value = 200)
	private int edad;
	
	/* JSON  {
	 *  "nombre": "Pedro",
	 *  "edad": 39
	 * }  */	
	public Alumno() {
		this.nombre = "";
		this.edad = 0;	// Es redundante, pero lo hacemos
	}	
	public Alumno(String nombre, int edad, String email) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.email = email;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
}
