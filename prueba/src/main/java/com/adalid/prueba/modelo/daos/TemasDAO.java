package com.adalid.prueba.modelo.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adalid.prueba.modelo.entidades.Alumno;
import com.adalid.prueba.modelo.entidades.Tema;

public interface TemasDAO
extends JpaRepository<Tema, Integer> {

}
