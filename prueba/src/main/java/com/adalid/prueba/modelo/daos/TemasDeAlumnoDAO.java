package com.adalid.prueba.modelo.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adalid.prueba.modelo.entidades.Tema;
import com.adalid.prueba.modelo.entidades.TemaDeAlumno;
import com.adalid.prueba.modelo.entidades.TemaDeAlumnoPK;

public interface TemasDeAlumnoDAO 
extends JpaRepository<TemaDeAlumno, TemaDeAlumnoPK> {

}
