package com.adalid.prueba.modelo.entidades;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class TemaDeAlumno implements Serializable  {

	 @EmbeddedId
	private TemaDeAlumnoPK idPk;
 
	 
	public TemaDeAlumno() {
		super();
	}

	public TemaDeAlumno(TemaDeAlumnoPK idPk) {
		super();
		this.idPk = idPk;
	}

	public TemaDeAlumnoPK getIdPk() {
		return idPk;
	}

	public void setIdPk(TemaDeAlumnoPK idPk) {
		this.idPk = idPk;
	}
	 
	 
}
