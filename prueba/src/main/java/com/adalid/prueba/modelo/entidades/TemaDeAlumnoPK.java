package com.adalid.prueba.modelo.entidades;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.sun.istack.NotNull;

// Clave Primaria, Primary Key
@Embeddable
public class TemaDeAlumnoPK implements Serializable  {

	@NotNull
	private Integer idAlumno;

	@NotNull
	private Integer idTema;

	
	public TemaDeAlumnoPK() {
		super();
	}

	public TemaDeAlumnoPK(Integer idAlumno, Integer idTema) {
		super();
		this.idAlumno = idAlumno;
		this.idTema = idTema;
	}

	public Integer getIdAlumno() {
		return idAlumno;
	}

	public void setIdAlumno(Integer idAlumno) {
		this.idAlumno = idAlumno;
	}

	public Integer getIdTema() {
		return idTema;
	}

	public void setIdTema(Integer idTema) {
		this.idTema = idTema;
	}
	
	
	
}
