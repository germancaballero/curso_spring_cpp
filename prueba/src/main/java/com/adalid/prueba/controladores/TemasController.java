package com.adalid.prueba.controladores;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adalid.prueba.modelo.daos.TemasDAO;
import com.adalid.prueba.modelo.entidades.Tema;

@RestController()
@RequestMapping("api/json/temas")
public class TemasController {

	@Autowired
	TemasDAO dao;
	
	@PostMapping()
	public Tema crearUsuario(@RequestBody Tema tema) {	
		// Recibe sin ID en el BODY de la petición HTTP y deserializa el JSON a un obj Usuario
		System.out.println("Tema: " + tema.getNombre());
		return dao.save(tema);	// Devuelve con ID
	}

	@RequestMapping(value="/{id}", method = {RequestMethod.GET /*, RequestMethod.POST */} )
	public Tema getTema(@PathVariable Integer id) {
		System.out.println(">>>> GET Tema - ID RECIBIDO " + id);

		Optional<Tema> tema = dao.findById(id);
		return  tema.orElse(null);
	}
	
}
