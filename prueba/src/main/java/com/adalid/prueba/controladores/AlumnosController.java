package com.adalid.prueba.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adalid.prueba.modelo.daos.AlumnosDAO;
import com.adalid.prueba.modelo.daos.TemasDeAlumnoDAO;
import com.adalid.prueba.modelo.entidades.Alumno;
import com.adalid.prueba.modelo.entidades.TemaDeAlumno;
import com.adalid.prueba.servicios.ServicioAlumnos;

@RestController()
@RequestMapping("api/json/alumnos")
public class AlumnosController {

	// Le pedimos a Spirng que nos haga la inyección de depencias, ed decir:
	// Como esta esta clase demende de AlumnosDAO y necesitamos uno nuevo,
	// Spring lo crea por nosotros y lo "inyecta", es decir, lo asigna de manera automática
	// AlumnosDAO alumnosDAO;
	
	// Spring usa el patrón Singleton (en toda la app hay una única instancia)
	// Que Spring se encarga de pasar allá donde se requiera (con @Autowired)
	@Autowired	
	ServicioAlumnos servicio;
	
	@Autowired
	TemasDeAlumnoDAO daoTA;
	
	// Devuelve un JSON: JavaScript Object Notation
	@GetMapping 
	java.util.List<Alumno> pruebaAlumno() { 
		System.out.println(">>> Listar todos");
		return servicio.getDAO().findAll();
	}
	// URL total: api/json/alumnos/formulario
	// Estos parámetros se enviarían desde un formulario HTML, con el método POST
	@PostMapping(value = "/formulario")
	public Alumno crearAlumnoPorParametros(
			@RequestParam String nombre, 
			@RequestParam Integer edad,
			@RequestParam String email)
	{
		Alumno nuevoAlum = new Alumno(nombre, edad, email);
		System.out.println(">>>> crearAlumnoPorParametros: " + nombre);
		return servicio.save(nuevoAlum);
	}
	// URL total: api/json/alumnos/
	// Ahora recibiremos un JSON {"nombre": "Fulanito", "edad": 20}
	// Que viene en el cuerpo de la petición HTTP.
	// Que Spring se encargará de parsear automáticamente mediante una librería llamada Jackson
	// e instanciar (crear new) el alumno
	@PostMapping
	public Alumno crearAlumnoPorJSON(@RequestBody Alumno nuevoAlumno) {
		System.out.println(">>>> crearAlumnoPorJSON: " + nuevoAlumno.getNombre());
		return servicio.save(nuevoAlumno);
	}
	// Vamos a coger una variable de ruta
	
	@DeleteMapping(value="/{id}")
	public void eliminarAlumno(@PathVariable Integer id) {
		System.out.println(">>>> ELIMINAR ID RECIBIDO " + id);
		servicio.getDAO().deleteById(id);
	}
	// Aquí recibimos tanto una variable de ruta como los nuevos datos del alumno en formato JSON
	// en el cuerpo de la petición HTTP, y los guardamos
	@PutMapping(value="/{id}")
	// @RequestMapping(value="/{id}", method = RequestMethod.PUT )
	public Alumno modificarAlumno(
			@PathVariable Integer id,
			@RequestBody Alumno nuevosDatosAlumno)
	{
		nuevosDatosAlumno.setId(id);
		System.out.println(">>>> MODIFICAR ID RECIBIDO " + id);
		servicio.getDAO().save(nuevosDatosAlumno);
		return nuevosDatosAlumno;
	}	

	// URL total: api/json/alumnos/
	// Ahora recibiremos un JSON {"nombre": "Fulanito", "edad": 20}
	// Que viene en el cuerpo de la petición HTTP.
	// Que Spring se encargará de parsear automáticamente mediante una librería llamada Jackson
	// e instanciar (crear new) el alumno
	@PostMapping(value="/temas")
	public TemaDeAlumno crearTemaDeAlumno(@RequestBody TemaDeAlumno temaDeAlumno) {
		System.out.println(">>>> crearTemaDeAlumno: " + temaDeAlumno.toString());
		return daoTA.save(temaDeAlumno);
	}

	// Devuelve un JSON: JavaScript Object Notation
	@GetMapping (value="/temas")
	java.util.List<TemaDeAlumno> listarTemasDeAlumno() { 
		System.out.println(">>> Listar todos temas de al");
		return daoTA.findAll();
	}
}
