package com.adalid.prueba;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import com.adalid.prueba.modelo.daos.AlumnosDAO;
import com.adalid.prueba.modelo.entidades.Alumno;

// Esta clase testea nuestra app
/* Nuestro test */
//@SpringBootTest
//@RunWith(SpringRunner.class)
@DataJpaTest
class PruebaApplicationTests {

	@Autowired
	public AlumnosDAO gestorAlumnos;
		
	@Test
	void contextLoads() {
		// gestorAlumnos = new AlumnosDAO();
		Alumno yo = new Alumno();
		yo.setNombre("Germán");
		yo.setEdad(37);
		yo.setEmail("asdfsdfs@fgfgf.fgs");
		int miId = gestorAlumnos.saveAndFlush(yo).getId();
		
		//gestorAlumnos.saveAndFlush(new Alumno("Juan Luis", 0));
		// gestorAlumnos.getAlumno(1).setNombre("Juan Luis");

		//gestorAlumnos.saveAndFlush(new Alumno("Oscar E. Estraño", 38));
		// gestorAlumnos.alumnos.get(2).nombre = "Oscar E. Estraño";
		// Como un SELECT * FROM ALUMNO
		
		try {
			Alumno malAlumno = new Alumno(null, -1, "sdffd@dfdf.cd");		
			assertTrue(comprobarAlumno(malAlumno));
			gestorAlumnos.saveAndFlush(malAlumno);
			assertTrue(comprobarAlumno(new Alumno("", 1000, "sdffd@dfdf.cd")));
			gestorAlumnos.saveAndFlush(new Alumno("", 1000, "sdffd@dfdf.cd"));
			// Aquí no debería llegar nunca, debe fallar
			fail("No ha fallado, no ha hecho bien la validación");
		} catch (Exception e) {
			System.out.println("Ok, ha fallado y debía fallar");
			
		}		
		//TODO: El siguiente funcionaba pero ha dejado de funcionar
		// por que hay que limpiar la sesión session.clear();
		/*List<Alumno> listaAlumnos = gestorAlumnos.findAll();
		// listaAlumnos.size()
		for (int i = 0; i < gestorAlumnos.count(); i = i + 1) {
			System.out.println("Nombre alumno: " + listaAlumnos.get(i).getNombre()
					+ ", edad = " + listaAlumnos.get(i).getEdad());
		}*/
		assertEquals("Germán", gestorAlumnos.findById(miId).orElse(null).getNombre());
		//assertEquals(37, gestorAlumnos.findById(1).orElse(null).getEdad());
		//assertEquals("Juan Luis", gestorAlumnos.findById(2).orElse(null).getNombre());
		//assertEquals("Oscar E. Estraño", gestorAlumnos.findById(3).orElse(null).getNombre());
	}
	boolean comprobarAlumno(Alumno alumno) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Alumno>> violations = validator.validate(alumno);
		for (ConstraintViolation<Alumno> constraintViolation : violations) {
			System.out.println(constraintViolation.toString());
		}
		return violations.size() > 0;
	}
}
