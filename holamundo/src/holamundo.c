/*
 ============================================================================
 Name        : holamundo.c
 Author      : Germ�n Caballero Rodr�guez
 Version     :
 Copyright   : Adalid Formaci�n
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    printf("Hola mundo!\n");

    printf("Muestra numero: %d\n", 100);

    printf("Muestra caracter: %c\n", 'X');
    float un_decimal = 44545.34343343434333;
    double muchos_digitos = 1.2345678901234567890;
    printf("Muestra array de char: %s , %lf,   \n", "TEXTO", un_decimal, muchos_digitos);

    long entero_largo1 = 12345678;
    long entero_largo2 = 123456789123456789;
    printf ("%ld y %ld", entero_largo1, entero_largo2);
    char caracter = getchar();
    printf("Muestra char: %c\n", caracter);
    // Con valores primitivos, se COPIA el VALOR (un clon)
    entero_largo2 = entero_largo1;
    entero_largo1 = -10;
    printf ("%ld y %ld\n", entero_largo1, entero_largo2);

    // Los punteros se declaran con el tipo y *
    int* ptr_ent_largo1, ptr_ent_largo2;
    ptr_ent_largo1 = &entero_largo1;    // El ampersand es LA DIRECCI�N DE MEMORIA de una variable
    ptr_ent_largo2 = &entero_largo2;

    ptr_ent_largo2 = ptr_ent_largo1;
    entero_largo1 = -10;
    printf ("Simbolos:  \r\n \t \%\n");
    printf ("Valores:  %ld y %ld\n", entero_largo1, entero_largo2);
    printf ("Punteros:  %ld y %ld\n", ptr_ent_largo1, ptr_ent_largo2);
    // Para mostrar el contenido de un puntero,
    //usamos el operador de indirecci�n como prefijo: *
    // s�lo, cuando no declaramos una variable
    printf ("Punteros:  %d y %d\n", *ptr_ent_largo1, *ptr_ent_largo2);

    /*
    char texto[20];

    strcpy( texto, "Texto culquiera");
    printf("Muestra array char: %s\n", texto);

    char* elMismoTexto;
    elMismoTexto = texto;
    printf("Muestra puntero a char: %s\n", elMismoTexto);
*/

    getchar();
    return 0;
}
