#include <stdio.h>
#include <stdlib.h>
#include <mem.h>

struct sesagesimal {
    float grados;
    float min;
    // float seg;
};

struct posGPS {
    struct sesagesimal latitud;
    struct sesagesimal *longitud;   // CUIDAO, es PUNTERO
    float altura;
};
struct ciudad {
    char nombre[40];
    struct ciudad *amiga;
    struct posGPS;
};

void probando_structs() {
    struct posGPS posMadrid;
    struct sesagesimal latitud;
    latitud.grados = 40;
    latitud.min = 23;
    posMadrid.latitud = latitud;

     posMadrid.longitud = malloc(sizeof (struct sesagesimal));
    ///posMadrid.longitud = 23434343;
    posMadrid.longitud->grados = 3;
    posMadrid.longitud->min = -50;


    mostrar_pos_gps_por_valor(posMadrid);

    printf("Despu�s de cero. Latitud: %f, Longitud: %f, Altura: %f\n",
           posMadrid.latitud, posMadrid.longitud, posMadrid.altura);

    mostrar_pos_gps_por_direcc_mem( & posMadrid  );

    printf("Despu�s de cero. Latitud: %f, Longitud: %f, Altura: %f\n",
           posMadrid.latitud, posMadrid.longitud, posMadrid.altura);

    free(posMadrid.longitud);

    // Si reservamos memoria y no liberamos, se acumula y puede llegar a cascar el programa
    for (long long int i = 0; i < 1000000; i++) {
        struct ciudad * daIgual = malloc(sizeof(struct ciudad));
    }
}
// Paso por valor, se hace una copia de los valores de la estructura
// Y se pasan a la funcion. Es decir, se hace un clon
void mostrar_pos_gps_por_valor(struct posGPS posicion) {
    printf("Latitud: %f � %f ', Longitud: %f, Altura: %f\n",
           posicion.latitud.grados, posicion.latitud.min,
           posicion.longitud->grados, posicion.longitud->min,
           posicion.altura);
    posicion.altura = 0;
}
// Pasando una direcci�n de memoria (puntero) S� PODEMOS CAMBIAR los valores
// originales dentro de la funci�n
void mostrar_pos_gps_por_direcc_mem(struct posGPS *posicion) {
    printf("Latitud: %f � %f ', Longitud: %f, Altura: %f\n",
           posicion->latitud.grados, posicion->latitud.min,
           posicion->longitud->grados, posicion->longitud->min, posicion->altura);
    posicion->altura = 0;
}
