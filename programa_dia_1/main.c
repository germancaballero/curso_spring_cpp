#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include "estructuras.h"
#include "definiciones.h"

void tipos_datos_primitivos(void);  // DECLARACI�N: S�lo decimos que existe
void tipos_datos_punteros(void);
void punteros_y_arrays(void);
void tipos_primitivos_tam_predet(void);

#define CODIGO_EXTRA
// #define WINDOW_7

#ifdef CODIGO_EXTRA
void funcionCodigoExtra() comienzo
    printf("Se ha incluido funcionCodigoExtra()\n");
fin
#endif // CODIGO_EXTRA

int main()
{
    printf("Hola mundo!\n");

#ifdef CODIGO_EXTRA
    funcionCodigoExtra();
#else
    printf("No se ha definido CODIGO_EXTRA\n");
#endif // CODIGO_EXTRA
    escribir(TEXTO_PI, PI);
    printf("PI = %f \n", 3.14159265);
    printf_float("PI", PI);

    // tipos_datos_primitivos();       // LLAMADA: S�lo invocamos a la funci�n
    // tipos_datos_punteros();
    // punteros_y_arrays();
    // tipos_primitivos_tam_predet();  // C99
    probando_structs();
    getchar();
    return 0;
}
// IMPLEMENTACI�N: Estamos implementando la funci�n
inline void  tipos_datos_primitivos(void)
{
    printf("Muestra numero: %d\n", 100);

    printf("Muestra caracter: %c\n", 'X');
    float un_decimal = 44545.34343343434333;
    double muchos_digitos = 1.2345678901234567890;
    printf("Muestra array de char: %s , %lf,   \n", "TEXTO", un_decimal, muchos_digitos);

    /*long long */int entero_largo1 = 12345678;
    /*long long*/ int entero_largo2 = 123456789123456789;
    printf ("%ld y %ld", entero_largo1, entero_largo2);
    char caracter = getchar();
    printf("Muestra char: %c\n", caracter);
    // Con valores primitivos, se COPIA el VALOR (un clon)
    entero_largo2 = entero_largo1;
    entero_largo1 = -10;
    printf ("%ld y %ld\n", entero_largo1, entero_largo2);
}

void tipos_datos_punteros()
{
    /*long long */int entero_largo1 = 12345678;
    /*long long*/ int entero_largo2 = 123456789123456789;

    // Los punteros se declaran con el tipo y *
   /* long long*/ int *ptr_ent_largo1, *ptr_ent_largo2;
    ptr_ent_largo1 = &entero_largo1;    // El ampersand es LA DIRECCI�N DE MEMORIA de una variable
    ptr_ent_largo2 = &entero_largo2;

    // ptr_ent_largo2 = ptr_ent_largo1;
    ptr_ent_largo2 ++;  // Al incrementar en 1 el puntero,
    // en realidad incrementamos en 1 unidad de memoria del tipo en cuesti�n
    // 4 bytes para int � float, 8 bytes para long long int � para double

    entero_largo1 = -10;
    printf ("Simbolos:  \r\n \t \%\n");
    printf ("Valores:  %ld y %ld\n", entero_largo1, entero_largo2);
    printf ("Punteros:  %ld y %ld\n", ptr_ent_largo1, ptr_ent_largo2);
    // Para mostrar el contenido de un puntero,
    //usamos el operador de indirecci�n como prefijo: *
    // s�lo, cuando no declaramos una variable
    printf ("Contenidos de punteros:  %d y %d\n", *ptr_ent_largo1, *ptr_ent_largo2);
}

void punteros_y_arrays() {

    char texto[20];

    strcpy( texto, "Texto cualquiera");
    printf("Muestra array char: %s\n", texto);
    char* ptr_texto;
    ptr_texto = texto;   // No hace falta el ampersand, un array es en s� una direcc. mem.
    printf("Muestra puntero a char: %s\n", ptr_texto);   // No hace falta el asterisko

    printf("Caracter posicion 3 = %c \n", texto[2]);
    printf("Caracter posicion 3 = %c \n", ptr_texto[2]);   // Los punteros se pueden usar como arrays

    printf("Caracter posicion 1 = %c \n", *texto);           // Los arrays se pueden usar como punteros
    printf("Caracter posicion 1 = %c \n", *ptr_texto);

    printf("Caracter posicion 10 = %c \n", *(texto + 9));       // Los arrays puden usar la
    printf("Caracter posicion 10 = %c \n", *(ptr_texto + 9));   // aritm�tica de punteros

    // Lo que no podemos hacer es:
    // texto = ptr_texto;//error: assignment to expression with array type|
}
void tipos_primitivos_tam_predet() {
    // los char ocupan 8 bits, 1 byte.   Va de -128 a 127
    char mini_entero = 65,  // 65 es la letra A en ASCII
         caracter = 'B';    // La 'B' es 66 en ASCII

    printf("Como caracteres:  %c  y  %c \n", mini_entero, caracter);
    // %d � %i representan un entero. %ld es un long
    printf("Como enteros:  %d  y  %i \n", mini_entero, caracter);

    // Los int16_t  ocupan 16 bits, 2 bytes
    // Van de -32768 a 32768
    int16_t entero_corto = 100;
    short int entero_corto_2 = 1000;

    printf("Como enteros:  %d  y  %i \n", entero_corto, entero_corto);
    // -2,147,483,648 a +2,147,483,647
    int32_t entero_normal = 10000000;
    int entero_normal2 = 10000000;

    printf("Como enteros:  %d  y  %i \n", entero_normal, entero_normal2);
    // Entero largo, 64 bits: -18446744073709551616 a 18446744073709551616
    int64_t entero_largo1 = 1232323232323;
    long long int ent_largo2 = 232323232323;

    printf("Como enteros:  %ld  y  %li \n", entero_largo1, ent_largo2);
    // Todos los tipos pueden ser SIN SIGNO: prefijo unsigned
    unsigned char caracter2;    // de 0 a 255
    unsigned int entero_positivo;   // de 0 a 65535
    uint32_t entero_pos32;   // de 0 a 65535
    printf("Como enteros:  %u  y  %u \n", caracter2, entero_positivo);

}
